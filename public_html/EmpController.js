/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


var myApp = angular.module('myApp',[]);
 

var uid = 1;
 
 
function EmpController($scope) {
    $scope.curPage = 0;
   $scope.pageSize = 2;
    $scope.emps = [
        { 'id':0, 'name': 'Anmol',
          'address':'Vavol',
          'city': 'Gandhinagar',
          'department': 'IT',
          'salary' : '75000',
          'other' : 'Good'
        },
        { id:1, 'name': 'Akanksha',
          'address':'Vavol',
          'city': 'Gandhinagar',
          'department': 'IT',
          'salary' : '75000',
          'other' : 'Good'
        },
        { id:2, 'name': 'Keenal',
          'address':'Vavol',
          'city': 'Gandhinagar',
          'department': 'IT',
          'salary' : '75000',
          'other' : 'Good'
        }
        
    ];
    
    //for pagination
    $scope.numberOfPages = function() {
                return Math.ceil($scope.emps.length / $scope.pageSize);
            };

     
    $scope.saveEmp = function() {
         
        if($scope.newemp.id == null) {
        //if this is new emp, add it in emps array
        $scope.newemp.id = uid++;
        $scope.emps.push($scope.newemp);
        } else {
        //for existing emp, find this emp using id
        //and update it.
        for(i in $scope.emps) {
            if($scope.emps[i].id == $scope.newemp.id) {
            $scope.emps[i] = $scope.newemp;
            }
        }                
        }
         
        //clear the add emp form
        $scope.newemp = {};
    }
 
     
    $scope.delete = function(id) {
         
        //search emp with given id and delete it
        for(i in $scope.emps) {
            if($scope.emps[i].id == id) {
                $scope.emps.splice(i,1);
                $scope.newemp = {};
            }
        }
         
    }
     
     
    $scope.edit = function(id) {
    //search emp with given id and update it
        for(i in $scope.emps) {
            if($scope.emps[i].id == id) {
                //we use angular.copy() method to create
                //copy of original object
                $scope.newemp = angular.copy($scope.emps[i]);
            }
        }
    }
}
//myApp.config(['$routeProvider',
//        function($routeProvider) {
//            $routeProvider.
//               when('/show', {
//                        templateUrl: 'show.html',
//                        controller: 'EmpController'
//                    }).
////                    when('/route2/:emp.id', {
////                        templateUrl: 'edit.html',
////                        controller: 'RouteController'
////                    }).
//                    otherwise({
//                        redirectTo: '/index'
//                    });
//        }]);
    
  
  //Filter for first letter capital  
 myApp.filter('capitalize', function() {

  // Create the return function and set the required parameter as well as an optional paramater
  return function(input, char) {

    if (isNaN(input)) {

      // If the input data is not a number, perform the operations to capitalize the correct letter.
      var char = char - 1 || 0;
      var letter = input.charAt(char).toUpperCase();
      var out = [];

      for (var i = 0; i < input.length; i++) {

        if (i == char) {
          out.push(letter);
        } else {
          out.push(input[i]);
        }
        
      }

      return out.join('');

    } else {
      return input;
    }

  }

});

//for directive of salary
myApp.controller('MainCtrl', function($scope) {
    });

    myApp.directive('validNumber', function() {
      return {
        require: '?ngModel',
            link: function(scope, element, attrs, ngModelCtrl) {
          if(!ngModelCtrl) {
            return;
          }

          ngModelCtrl.$parsers.push(function(val) {
            if (angular.isUndefined(val)) {
                var val = '';
            }
           
            var clean = val.replace(/[^0-9\.]/g, '');
            var negativeCheck = clean.split('-');
            var decimalCheck = clean.split('.');
            if(!angular.isUndefined(negativeCheck[1])) {
                negativeCheck[1] = negativeCheck[1].slice(0, negativeCheck[1].length);
                clean =negativeCheck[0] + '-' + negativeCheck[1];
                if(negativeCheck[0].length > 0) {
                    clean =negativeCheck[0];
                }
               
            }
             
            if(!angular.isUndefined(decimalCheck[1])) {
                decimalCheck[1] = decimalCheck[1].slice(0,2);
                clean =decimalCheck[0] + '.' + decimalCheck[1];
            }

            if (val !== clean) {
              ngModelCtrl.$setViewValue(clean);
              ngModelCtrl.$render();
            }
            return clean;
          });

          element.bind('keypress', function(event) {
            if(event.keyCode === 32) {
              event.preventDefault();
            }
          });
        }
      };
    });
    
  //for pagination  
angular.module('myApp').filter('pagination', function()
{
 return function(input, start)
 {
  start = +start;
  return input.slice(start);
 };
});


myApp.controller('myCtrl', function($scope) {
  $scope.name = 'World';
   $scope.sample = "Sample";
    $scope.getArray = [{anm: 1, bgh:2}, {anm:3, bgh:4}];         
});

myApp.directive('fileReader', function() {
  return {
    scope: {
      fileReader:"="
    },
    link: function(scope, element) {
      $(element).on('change', function(changeEvent) {
        var files = changeEvent.target.files;
        if (files.length) {
          var r = new FileReader();
          r.onload = function(e) {
              var contents = e.target.result;
              scope.$apply(function () {
                scope.fileReader = contents;
              });
          };
          
          r.readAsText(files[0]);
        }
      });
    }
    
  };
  
});


